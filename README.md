# Portable Animal Encyclopedia
This project is a Portable Animal Encyclopedia built using Python and the Tkinter library for the GUI. It fetches information from Wikipedia using their public API to provide educational content about various animals.
## Getting Started

To get started with this project, clone the repository to your local machine and ensure you have Python installed.
Prerequisites

    Python 3.x
    pip (Python package manager)

## Installation

    git clone https://gitlab.com/rebeldoomer/animal-encyclopedia.git

Navigate to the cloned directory:

    cd animal-encyclopedia

Install required Python packages:

    pip install -r requirements.txt

Run the application:

    python main.py

## Features

Fetches and displays animal profiles from Wikipedia.
Caches animal profiles to reduce API calls.
Allows users to switch between a light and dark theme.
Displays detailed information and images for each animal.

## Usage

Upon launching the application, you will be presented with the main window where you can view animal profiles, switch themes, and learn more about the project in the "About" section.
## Support

For support, please open an issue in the GitLab repository.
Contributing

Contributions are welcome! Please feel free to fork the project, make changes, and submit a merge request.

## License
 This work is licensed under Attribution-NonCommercial-ShareAlike 4.0 International 

## Project Status

This project is in its initial stages and will continue to be developed. Feature requests and contributions are welcome.